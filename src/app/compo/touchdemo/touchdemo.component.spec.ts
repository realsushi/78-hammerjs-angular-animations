import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TouchdemoComponent } from './touchdemo.component';

describe('TouchdemoComponent', () => {
  let component: TouchdemoComponent;
  let fixture: ComponentFixture<TouchdemoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TouchdemoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TouchdemoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
