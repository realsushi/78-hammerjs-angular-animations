# SampleApplication

## Abstract
This project is a fork of an Angular tutorial for **Hammer.JS**. Which is an incredibly popular lib on Github!
It has been updated slightly from the source repo.

## Angular
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.2.11.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

## Run local version of Angular
To run a specific version of Angular, we would need to install a local version. 
- `npm i`
- `npm i @angular/cli@11.2.12`
- `./node_modules/@angular/cli/bin/ng serve` 
A local version module needs to have its path specified as it won't be available globally.

## Deployment and hosting
Hosting is done on Firebase as it relatively well integrated being Google stuff. It is an SPA.
- `./node_modules/firebase-tools/lib/bin/firebase.js login`
- ./node_modules/firebase-tools/lib/bin/firebase.js init
- npm run build
- ./node_modules/firebase-tools/lib/bin/firebase.js deploy 

## Common issues
- openssl issue
	- error crypto hash digital envelope routines::unsupported
	- workaround `export NODE_OPTIONS=--openssl-legacy-provider`

